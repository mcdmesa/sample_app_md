class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
    add_index :users, :email, unique: true
  end
end
#this uses a rails method called add_index to add an index on the email column of the users table. the index by itslef doesn't enforce uniqueness, but the option unique: true does.
